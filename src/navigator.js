var exports 		= {},
	landingURL 		= window.location.pathname, //	Might come in handy...
	callbacks 		= observable({}),
	Navigator = bindAll(observable({
		hist: [],
		RouteConfig: {},
		routes: {},

		start 		: initialize,
		popState 	: onPopState,
		pushState 	: pushRouteState,
		route 		: addRoute,
		to 			: navigate,
		back 		: back
	}));

//	handle browser forward/backward buttons
function onPopState() {
	if(window.location.pathname === this.hist[this.hist.length-1]) this.hist.pop();
	this.to(this.hist[this.hist.length-1] || window.location.pathname, true);
}

//	Triggers popstate, and routes as normal...
function back() {
	window.history.back(-1);
}

/**
 *	Function `pushRouteState`
 *	This is primarily used by the NavigatorEvent -- given a path, and an optional
 *	replace parameter, the method will determine how to handle the URL and its
 *	place in the Navigator `hist` stack.
 */
function pushRouteState(navigatorEvent, replace) {
	var route = navigatorEvent;

	if(replace) window.history.replaceState({}, result(route.title), route);
	//	If we're not replacing, push this guy into the local Navigator history stack
	if(!replace) {
		if(this.hist[this.hist.length-1] === route) {
			this.hist.pop();
		}
		else if(this.hist[this.hist.length-1] !== window.location.pathname) {
			this.hist.push(window.location.pathname);
		}

		window.history.pushState({}, result(route.title), route);
	}

	if(this.hist) this.hist = [].concat(this.hist);
}

//	parse options, if dispatch hasn't explicitely been prevented, navigate the
//	current `pathname`, and THEN assign the window popstate.
function initialize(options) {
	this.config = options;
	if(options.routes) for(var i in options.routes) this.route(options.routes[i])
	if(options.dispatch !== false) this.to(window.location.pathname, true);
	this.trigger('start', this);
	window.onpopstate = this.popState;
	this.on('route', this.pushState);
}

/**
 *	Function `addRoute`
 *		addRoute(route, callback...)
 *		addRoute(title, route, callback...)
 *
 *	The `title` parameter is optional, and if included, will be used only to set
 *	the active route context paramter `title`, in which case a provided callback
 *	can just call `document.title = this.title`.
 *
 *	Routes can be exact strings, strings with required or optional named parameters,
 *	OR strings with regular expression named paramters. Examples below:
 *		/blog 							--	no parameters
 *		/blog/articles/:article_id		--	with a reqired parameter
 *		/blog/articles/:article_id?		-- 	with an optional parameter
 *		/blog/articles/:month=(\\w+)	-- 	with a required regexp paramter
 *
 *	* Non-regular expression parameters will match any number of valid URL characters
 */
function addRoute(route, callback/*[, callback...]*/) {
	var route = route, title, config;

	//	Handle both configuration object and a simple route...
	if(Object.prototype.isPrototypeOf(arguments[0])) {
		var config = route;
		title = config.title;
		route = config.path;
	}
	else {
		config = {path: route};
	}

	//	Matches a string like '/directory/:ggp=(\\w+)/:malls?'
	//	and if we `match` its output is [":ggp=(\\w+)", ":malls?"]
	var pattern 	= route,
		//	match all groups (including their optional/regexp flags)
		groups 		= pattern.match(/:([-\w_\d]+)(=[\-\\\[\]\w\d\+\*\?\(\)\{\}\,\s]+)?\??/g),
		//	string-ized regexp of ALL valid URL characters
		UrlRE 		= "([-\\w\\d\\.\\_\\~\\:\\/\\?\\#\\[\\]\\@\\!\\$\\&\\'\\(\\)\\*\\+\\,\\;\\=]+)",
		//	since the title is optional, we need to determine where to slice the arguments for callbacks...
		callbacks 	= config.callbacks || Array.prototype.slice.call(arguments, 1);

	for(var g in groups) {
		var routeParams = groups[g].split(/(\?|\=)/), 	// split on optional or regexp paths
			groupRE 	= groups[g];

		if(!!routeParams[2]) {
			groupRE = routeParams[2];  // route regexp value
		}
		else {
			groupRE = UrlRE;
		}

		if(~routeParams.indexOf('?')) 	groupRE += '?';  // if optional ignore preceeding '/' before param
		pattern = pattern.replace(groups[g], '?' + groupRE); 	// overwrite group in route pattern with the real RE (also, make preceeding '/' optional)

		groups[g] = {
			name 		: routeParams[0].substr(1), 	// get rid of preceeding ':'
			isOptional	: !!~routeParams.indexOf('?'),
			isRegExp 	: !!routeParams[2],
			re  		: new RegExp(groupRE)
		};
	}

	this.routes[pattern] = {
		config  	: config,
		path 		: route,
		groups 		: groups,
		callbacks 	: callbacks
	};

	return this;
}

/**
 *	Function navigate
 *
 *	Attempts to match a given path to a registered route. The optional replace
 *	argument is a boolean, in which its value refers to invoking the `history.pushState`
 *	and `history.replaceState` methods.
 */
function navigate(route, replace) {
	var re, groups, params, values, evt, match,
		noRequiredGroupValue = false, hasRoute = false;

	//	If we're not replacing, don't re-route a route! Ya dummy!
	if(!replace && window.location.pathname === route) return;

	matches = [];

	for(var r in this.routes) {
		hasRoute = true;
		//	Create a RE out of our generated route RE
		re = new RegExp('^'+r+'$', 'g');
		if(re.test(route)) matches.push(this.routes[r]);
	}

	//	Sort the routes by most specific first.
	matches.sort(function(a, b) {
		if(a.path.length > b.path.length) return -1;
		if(a.path.length < b.path.length) return 1;
		return 0;
	});

	//	Go through each matched route until we find one that fulfills all properties
	if(matches.length) {
		for(var i in matches) {
			evt = createNavigatorEvent(this, matches[i]);

			groups 		= matches[i].groups;	//	Get all/any parameter groups
			routeChunks = route.split('/').slice(2);
			params 		= {}; 					// 	Our params object, for the NavigatorEvent
			values 		= route.match(re); 		//	The values for our params

			//	Get the values for our group names, and populate the params dictionary
			each(groups, function(group, i) {
				//	Match each groupname with it's respective match
				//	within the passed url.
				evt.params[group.name] = routeChunks[i] && routeChunks[i].match(group.re)[1] || null;

				//	If a non-optional parameter was not populated, tell navigator so...
				if(!evt.params[group.name] && !group.isOptional) noRequiredGroupValue = true;
			});

			if(noRequiredGroupValue) {
				//	Reset the variable
				noRequiredGroupValue = false;
				//	and continue on testing routes...
				continue;
			}

			this.trigger('before', evt);
			evt.init(route);
			//	Since we found a match, break on out (any other routes are discarded)
			break;
		}
	}
}