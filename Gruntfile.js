module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		concat: {
			all: {
				src: ['src/utils.js', 'src/navigatorEvent.js', 'src/navigator.js', 'src/configs/*.js', 'src/exports.js'],
				dest: './<%= pkg.name %>.js'
			}
		},

		uglify: {
			options: {
				banner: ['//\t<%= pkg.name %> v<%= pkg.version %>\n',
						'//\thttps://bitbucket.org/punchkickinteractive/navigator\n',
						'//\t(c)<%= (new Date()).getFullYear() %> <%= pkg.author %>\n',
						'//\thttp://punchkickinteractive.com\n',
						'//\tNavigator may be freely distributed under the MIT license\n'].join('')
			},
			all: {
				files: {
					'./<%= pkg.name %>-min.js': './<%= pkg.name %>.js',
					'./dist/<%= pkg.name %>-amd-min.js': '<%= module.amd.dest %>',
					'./dist/<%= pkg.name %>-common-min.js': '<%= module.common.dest %>'
				}
			}
		},

		watch: {
			default: {
				files: ['<%= concat.all.src %>'],
				tasks: ['default']
			}
		},

		module: {
			options: {
				banner: '<%= uglify.options.banner %>'
			},
			amd: {
				files: ['./<%= pkg.name %>.js'],
				dest: './dist/<%= pkg.name %>-amd.js'
			},
			common: {
				files: ['./<%= pkg.name %>.js'],
				dest: './dist/<%= pkg.name %>-common.js'
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	function requireModule(src) {
		return ['define(function() {',
					'\t' + src.split('\n').join('\n\t'),
				'});'].join('\n');
	}

	function commonModule(src) {
		src = src.slice(0,src.lastIndexOf('return')) + 'module.exports = ' + src.slice(src.lastIndexOf('return')+6);
		return ['(function() {',
					'\t' + src.split('\n').join('\n\t'),
				'})();'].join('\n');
	}

	function noModule(src) {
		src = src.slice(0,src.lastIndexOf('return')) + '\nscope.Navigator = ' + src.slice(src.lastIndexOf('return')+6);
		return ['(function(scope) {',
					'\t' + src.split('\n').join('\n\t'),
				'})(window);'].join('\n');
	}

	grunt.registerMultiTask('module', 'Builds the project in the specified module format', function() {
		var src = grunt.file.read(this.data.files[0]),
			opts = this.options() || {},
			dest;

		if(this.nameArgs === 'module:amd') {
			dest = this.data.dest || './dist/' + grunt.config('pkg.name') + '.amd.js';
			src = requireModule(src);
		}
		else if(this.nameArgs === 'module:common') {
			dest = this.data.dest || './dist/' + grunt.config('pkg.name') + '.common.js';
			src = commonModule(src);
		}

		grunt.file.write(dest, (opts.banner||'') + src);
		grunt.log.ok('Created ' + this.nameArgs.split(':').pop() + ' module at ' + dest);
	});

	grunt.registerTask('wrapper', function() {
		src = grunt.file.read('./' + grunt.config('pkg.name') + '.js');
		//	Wrap the module-spec-agnostic `navigator.js`
		grunt.file.write('./' + grunt.config('pkg.name') + '.js', grunt.config('uglify.options.banner') + noModule(src));

		//	Checks for --update flag
		//	ARGUMENTS ALLOWED
		//		--update p[atch] - updates patch version number (incrementally)
		//		--update min[or] - updates minor version number (incrementally)
		//		--update maj[or] - updates major version number (incrementally)
		//		--update 1 		 - updates the patch version number to '1'
		//		--update 3.1 	 - updates the minor version to '3' and the patch to '1'
		//		--update 4.3.1 	 - updates the major version to '4' the minor to '3' and the patch to '1'
		grunt.option.flags().forEach(function(f) {
			var tokens = f.split('=');
			if(tokens[0] === '--update') {
				var pkg 		= grunt.file.readJSON('package.json'),
					bower 		= grunt.file.readJSON('bower.json'),
					version 	= pkg.version.split('.'),
					hardVersion = tokens[1].split('.');

				if(hardVersion && parseInt(hardVersion[0], 10)) {
					var overrides = [3-hardVersion.length, hardVersion.length].concat(hardVersion);
					Array.prototype.splice.apply(version, overrides);
				}
				else {
					grunt.log.ok('Updating project version...');
					if(/p(atch)?/.test(tokens[1])) version[2]++;
					if(/min(or)?/.test(tokens[1])) version.splice(1,2,+version[1]+1,0);
					if(/maj(or)?/.test(tokens[1])) version.splice(0,3,+version[0]+1,0,0);
				}

				pkg.version = bower.version = version.join('.');

				grunt.file.write('package.json', JSON.stringify(pkg));
				grunt.file.write('bower.json', JSON.stringify(bower));
			}
		});
	});

	grunt.registerTask('default', ['concat:all', 'module', 'wrapper', 'uglify:all']);
	grunt.registerTask('amd', ['concat:all', 'module:require', 'wrapper']);
	grunt.registerTask('common', ['concat:all', 'module:common', 'wrapper']);
}