var assert 			= require('assert'),
	fs 				= require('fs'),
	Window			= require('./window'),
	window,
	Navigator;

function loadNavigator() {
	window = Window();
	with (window) eval(fs.readFileSync('dist/navigator-common.js').toString());
	Navigator = window.module.exports;
}

describe('Navigator', function() {
	describe('#start()', function() {
		var triggered;

		beforeEach(function() {
			triggered = false;
			loadNavigator();

			Navigator.route('/', function() {
				triggered = true;
			});
		});

		it('should not trigger a route if dispatch: false is defined', function() {
			Navigator.start({dispatch: false});
			assert.equal(triggered, false);
		});

		it('should trigger a route if dispatch is not defined', function() {
			Navigator.start({});
			assert.equal(triggered, true);
		});
	});

	describe('#to()', function() {
		beforeEach(function() {
			triggered = false;
			loadNavigator();

			Navigator.route('/', function() {
				triggered = true;
			});

			Navigator.route('/about', function() {
				triggered = true;
			});

			Navigator.start({});
		});

		it('should change the pathname to the given route', function() {
			Navigator.to('/about');
			assert.equal(window.location.pathname, '/about');
		});

		it('should correctly route the browser and the url via back', function() {
			Navigator.to('/about');
			assert.equal(window.location.pathname, '/about');
			Navigator.back();
			assert.equal(window.location.pathname, '/');
		});
	});

	describe('#route()', function() {
		beforeEach(function() {
			loadNavigator();
		});

		it('should handle basic named parameters correctly', function(done) {
			Navigator.route('/blog/:id', function(e) {
				assert.equal(e.params.id, 12);
				done();
			});

			Navigator.start({});
			Navigator.to('/blog/12');
		});

		it('should handle named regular expression parameters correctly', function(done) {
			Navigator.route('/blog/:id=(\\d+)', function(e) {
				assert.equal(e.params.id, 12);
				done();
			});

			Navigator.start({});
			Navigator.to('/blog/12');
		});

		it('should handle many basic named parameters correctly', function(done) {
			Navigator.route('/blog/:year/:month', function(e) {
				assert.equal(e.params.year, 2009);
				assert.equal(e.params.month, 11);
				done();
			});

			Navigator.start({});
			Navigator.to('/blog/2009/11');
		});

		it('should handle many named regular expression parameters correctly', function(done) {
			Navigator.route('/blog/:year=(\\d+)/:month=(\\w+)', function(e) {
				assert.equal(e.params.year, 2009);
				assert.equal(e.params.month, "january");
				done();
			});

			Navigator.start({});
			Navigator.to('/blog/2009/january');
		});

		it('should handle all valid route parameter types', function(done) {
			Navigator.route('/blog/:year/:month=(\\w+)/:day=(\\d+)', function(e) {
				assert.equal(e.params.year, "two-thousand-nine");
				assert.equal(e.params.month, "january");
				assert.equal(e.params.day, 12);
				done();
			});

			Navigator.start({});
			Navigator.to('/blog/two-thousand-nine/january/12');
		});

		it('should not handle a non-matching regular expression parameter', function(done) {
			var triggered = false;

			Navigator.route('/blog/:year=(\\d+)', function(e) {
				triggered = true;
			});

			Navigator.route('/', function() {
				assert.equal(triggered, false);
				done();
			});

			Navigator.start({ dispatch: false });

			Navigator.to('/blog/two-thousand-nine');
			Navigator.back();
		});

		it('should accept a configuration object', function(done) {
			Navigator.route({
				path: '/blog'
			}, function(e) {
				assert.equal(window.location.pathname, '/blog');
				done();
			});

			Navigator.start({});
			Navigator.to('/blog');
		})
	});

	describe('#route() with Dependencies', function() {
		beforeEach(function() {
			loadNavigator();
		});

		it('Should load the dependencies before the route is complete', function(done) {
			window._xhr.response = '{ "data": { "articles": [] }}';
			window._xhr.headers = {'Content-Type': 'application/json'};

			Navigator.route({
				path: '/articles',
				deps: [ 'http://foo.com/mydeps.json' ]
			}, function(e) {
				assert.equal(!!e.data.articles, true);
				done();
			});

			Navigator.start({ deps: { root: 'data' } });
			Navigator.to('/articles');
		});

		it('Should correctly handle optional named params in dependency URL', function(done) {
			window._xhr.response = '{"articles": true}';

			Navigator.route({
				path: '/articles/:id?',
				deps: [ 'http://foo.com/articles/:id' ],
			}, function() {});

			Navigator.start({});

			Navigator.on('deps:complete', function(data, text, xhr) {
				assert.equal(xhr.url.indexOf(':id'), -1);
				assert.equal(xhr.url.indexOf('articles/'), -1);
				done();
			});

			Navigator.to('/articles');
		});

		it('Should replace named parameters within a given dependency URL', function(done) {
			window._xhr.response = '{"articles": true}';

			Navigator.route({
				path: '/articles/:id',
				deps: [ 'http://foo.com/articles/:id' ]
			}, function() {});

			Navigator.start({});

			Navigator.on('deps:complete', function(data, text, xhr) {
				assert.equal(xhr.url.indexOf(':id'), -1);
				assert.equal(xhr.url.indexOf('articles/12') >= 0, true);
				done();
			});


			Navigator.to('/articles/12');
		});

		it('Should correctly trigger before request event', function(done) {
			window._xhr.response = '{"articles": true}';

			Navigator.route({
				path: '/articles/:id',
				deps: [ 'http://foo.com/articles/:id' ]
			}, function() {});

			Navigator.start({});

			Navigator.on('deps:before', function(e, dep) {
				dep.data = { data: true };
			});

			Navigator.on('deps:complete', function(data, text, xhr) {
				if(xhr.url.indexOf('?data=true') >= 0) done();
			});

			Navigator.to('/articles/12');
		});

		describe('dependency callbacks', function() {
			beforeEach(function() {
				loadNavigator();
			});

			it('Should execute dependency-specific error callbacks', function(done) {
				window._xhr.status = 500;

				Navigator.route({
					path: '/',
					deps: [ {
						url: 'http://foo.com/home.json',
						onerror: function() {
							done();
						}
					} ]
				}, function() {});

				Navigator.start({});
			});

			it('Should execute dependency-specific filter callbacks', function(done) {
				Navigator.route({
					path: '/',
					deps: [ {
						url: 'http://foo.com/home.json',
						oncomplete: function(e) {
							e.data.changed = true;
						}
					} ]
				}, function(e) {
					assert.equal(e.data.changed, true);
					done();
				});

				Navigator.start({});
			});

			it('Should execute global dependency filter', function(done) {
				var fired = false;

				Navigator.route({
					path: '/articles',
					deps: [ 'http://foo.com/mydeps.json' ]
				}, function(e) {
					assert.equal(e.data.changed, true);
					done();
				});

				Navigator.start({ dispatch: false });

				Navigator.on('deps:complete', function(e, text, xhr) {
					e.data.changed = true;
				});

				Navigator.to('/articles');
			});

			it('Should execute global dependency errors', function(done) {
				window._xhr.status = 500;

				var fired = false;

				Navigator.route({
					path: '/articles',
					deps: [ 'http://foo.com/mydeps.json' ]
				}, function() {});

				Navigator.start({ dispatch: false });

				Navigator.on('deps:error', function() {
					done();
				});

				Navigator.to('/articles');
			});
		});
	});
});