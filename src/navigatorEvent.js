/**
 * 	Function createNavigatorEvent
 *
 * 	Creates a navigatorEvent, which holds an active routes state. The event object
 *	itself doesn't affect the router globally, only the ACTIVE route. The event
 * 	object is used for any global callbacks (allowing them to stop/resume/cancel)
 *	routes that would otherwise be processed.
 */
function createNavigatorEvent(Navigator, routeConfig) {
	return bindAll({
		done 		: false, 			// Is this route complete?
		paused 		: false, 			// Is this route paused?

		title 		: null, 			// The `title` associated with this route (if a function, it will be evaluated upon `init`)
		hist 		: Navigator.hist,	// A copy of the history stack
		params 		: {}, 				// Dictionary of route parameters and values
		data 		: {}, 				// Data that is populated from any dependencies
		callbacks 	: [], 				// The callback stack for this route
		target 		: routeConfig,
		navigator 	: Navigator,

		toString: function() {
			//	return the path of the route when forced to string
			return this.path;
		},

		//	Initialize the event by providing the route (from `routes`) that is currently
		//	active.
		init: function(route) {
			this.path = route;
			for(var i in routeConfig.on) Navigator.on(i, routeConfig.on[i]);
			Navigator.trigger('init', this);
			for(var i in routeConfig.callbacks) this.callbacks.push(routeConfig.callbacks[i]);
			this.continue();
			return this;
		},

		//	Cancel the route -- this is used when a route dependency errors out.
		cancel: function() {
			Navigator.trigger('cancel', this);
			for(var i in this) this[i] = undefined;
		},

		//	Pause the current route in place -- the route will not be fulfilled until
		//	`resume` is called.
		pause: function() {
			if(this.done) return;
			if(!this.paused) this.paused = true;
		},

		//	Resumes the routing process if it has been paused
		resume: function() {
			if(this.done) return;
			if(this.paused) this.paused = false;
			this.continue();
		},

		//	Continue processing the routes callbacks
		continue: function() {
			if(this.paused) return;
			if(!this.callbacks.length) return this.complete();
			if(this.callbacks.length === routeConfig.callbacks.length) Navigator.trigger('route', this);
			this.callbacks.shift()(this);
			this.continue();
		},

		//	Officiate the completion of the route
		complete: function() {
			if(this.callbacks.length) return this.continue();
			Navigator.trigger('complete', this);
			for(var i in routeConfig.on) Navigator.off(i, routeConfig.on[i]);
			this.done = true;
		}
	});
}