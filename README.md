Navigator v0.4.3
===
### A small simple stand-alone and strictly modern* browser router for web apps
<sup>\*This means absulutely **NO** support for hashes, and requires both `history.pushState` and `history.replaceState`.</sup>

### Installation
```
	$> bower install Navigator
```

---

### Building
All tasks will output a fully-comments and un-minified `navigator.js` and uglified `navigator.min.js`

##### default
If you're not using any particular module spec, use this. By default, the namespace is
`window.Navigator`. Also both AMD and CommonJS compatible modules are created in the `dist` directory.

```
	$> grunt
```

##### amd
If you're using requirejs, use this build task. Outputs `navigator-require.js` to the
`dist` directory.
```
	$> grunt amd
```

##### cjs
If you're using CommonJS, use this build task. Outputs `navigator-common.js` to the `dist`
directory.
```
	$> grunt common
```

---

### Testing
**BE SURE TO HAVE MOCHA INSTALLED GLOBALLY!!!**
```
	$> mocha
```

### API

#### `Navigator.start([config])`
The optional config parameter should be an object with explicit configuration options.
Other configuration options include the option for an initial route, via `dispatch`,
and a more convenient interface for creating routes via `routes`.

```JavaScript
Navigator.start({
	dispatch: false,
	routes: [
		{
			path: '/blog',
			title: 'My Blog',
			deps: [ 'http://myblog.com/blogs' ],
			callbacks: [ callbackOne, callbackTwo ]
		}
	]
});
```

#### `Navigator.route(route, callbacks...)`
Registers a route. Route can be given as a string, or as a configuration object. The
callbacks are called in the order provided. Each callback is run within the context
of a `NavigatorEvent`, allowing one to gain control over the routing flow by allowing
calls to `this.pause()` and `this.resume()`.

```JavaScript
// Simple route
Navigator.route('/home', callbackOne, callbackTwo);

// Named parameter route
Navigator.route('/blog/:post_id', callbackOne, callbackTwo);

// Named optional parameter route
Navigator.route('/blog/:post_id?', callbackOne, callbackTwo);

// Named parameter with regular expression (be sure to escape escaped chars since it's a string!)
Navigator.route('/blog/:date=(\\d+\\-\\d+)', callbackOne, callbackTwo);

// Multiple named paramters
Navigator.route('/blog/:post_id/:post-date=(\\d+\\-\\d+)', callbackOne, callbackTwo);

// A configured route with a single dependency (the dependency will be loaded and added
// to the `this.data` attribute of the `NavigatorEvent` context). Also, this sets the
// `title` attribute of the `NavigatorEvent` based on the loaded dependency.
Navigator.route({
	path: '/blog/:id',
	deps: [ 'http://myapi.com/blog/:id' ],
	title: function(e) { return e.data.post.title; }
}, callbackOne, callbackTwo)
```

All callbacks are are passed a `NavigatorEvent` as their first argument, a callback
See [NavigatorEvent](#NavigatorEvent).
```JavaScript
Navigator.route('home', '/home', callbackOne);

function callbackOne(e) {
	e.title; 		// The current routes title (in this case, 'home')
}
```

####`Navigator.to(path[, replace])`
Parses the given path, and fires the respctive route's callbacks.

####`Navigator.trigger(eventName, data...)`
Triggers the given event, and in turn any registered callbacks

####`Navigator.on(eventName, callback)`
Registers the given callback for the given event.

####`Navigator.off(eventName/*, callback*/)`
Unregisters the given callback for the given event name **or** if no callbacks is given
removes **all** events.

See the [Events](#Events) section for more details.

### NavigatorEvent
The Navigator event is the core of Navigator, by listening in on Navigator events, one
can control the flow of routes by the `NavigatorEvent` interface.

####`NavigatorEvent.init(routeConfig)`
Given a routeConfig, the Navigator event will finish its configuration and begin running
the route's callback stack.

####`NavigatorEvent.pause()`
The `pause` method will stop the current routing, this is usually only relevant in a
global config callback (something given to `Navigator.start`).

```JavaScript
//	The NavigatorEvent can be controlled and is always passed as the first argument
//	for any routing event
Navigator.on('before', function(e) {
	e.pause();

	//	The route's callback stack will be initiated after 500ms
	setTimeout(function() {
		e.resume(); 	// resumes the route (will fire the callback stack!)
	}, 500);
}
});
```

####`NavigatorEvent.resume()`
The `resume` method will resume the current route (only has an effect if the event has
been paused).

####`NavigatorEvent.cancel()`
Cancel the current route and destroys the active `NavigatorEvent`. There is no resuming
from this state.

####`NavigatorEvent.continue()`
Continue on to the next callback in the route's callback stack.

####`NavigatorEvent.complete()`
Complete the current route (will not run if callbacks remain).

####`NavigatorEvent.title`
The title of the given route (defaults to the `document.title` at the time the route
was fired).

####`NavigatorEvent.params`
A dictionary of paramNames and their respective values for the current route.

```JavaScript
Navigator.route('blog post', '/blog/:post_id', callbackOne);

function callbackOne() {
	this.params.post_id; 	// will contain the value of post_id
}
```
####`NavigatorEvent.data`
If any dependencies are set for the route, the response will be part of the `NavigatorEvent`'s
`data` attribute. Also, root-level objects will be merged with this attribute.

####`NavigatorEvent.callbacks`
An array of the originally supplied callbacks for the route.

####`NavigatorEvent.paused`
The paused status of the active route.

####`NavigatorEvent.done`
Returns a boolean representing if the route has completed.

####`NavigatorEvent.hist`
A stack of paths the current session has tracked (last being latest).

####`NavigatorEvent.navigator`
A reference to the Navigator instance that created this NavigatorEvent

---

### Events
Events allow for convenient ways to control Navigator's routing flow. **Every** callback
is (and always should be) the active `NavigatorEvent`. The following events are currently
emitted by Navigator:

- `before(navgiatorEvent)`: Fired before a NavigatorEvent has been initiated
- `init(navgiatorEvent)`: Fired when a NavigatorEvent has been initiated (but before callbacks are added to the stack)
- `route(navgiatorEvent)`: Fired just before the first callback in the stack is called
- `complete(navgiatorEvent)`: Fired after a route has been completely processed (after all callbacks have cleared the stack)
- `cancel(navgiatorEvent)`: Fired, if for any reason a route is cancelled

#### Add-on Events
Anything can trigger events on a Navigator instance by calling its `trigger` methods.

- `deps:error(navigatorEvent)`: Fired when a dependency cannot be fulfilled
- `deps:complete(navigatorEvent)`: Fired when a dependency has been fulfilled
