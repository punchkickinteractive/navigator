require('http').createServer(function(req, res) {
    var content = [
        '<html>',
        '<head><title>TEST</title></head>',
        '<body>',
        '<script>',
        require('fs').readFileSync('../navigator.js'),
        '</script>',
        '</body>',
        '</html>'
    ];
    
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.write(content.join('\n'));
    res.end();
}).listen(3000);

console.log('HOSTING ON PORT 3000');
