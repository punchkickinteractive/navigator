Navigator.on('before', function(e) {
	if(e.target.config.deps) e.callbacks.push(requestDependencies);
});

/**
 *	Function `requestDependencies`
 *	Makes requests based on route `deps` values. The values can be a
 *	string (i.e. http://myapi.com/foo), or an object with more specifics pertaining
 *	to the AJAX call -- this outputs a curried call to the appropriate dependency call.
 *
 *	// Simple Dependency
 *	Navigator.route({
 *		path: '/foo',
 *		deps: [ 'http://myapi.com/foo' ]
 *	});
 *
 *  // Configured Dependency
 *	Navigator.route({
 *		path: '/foo',
 *		deps: [{
 * 			url: 'http://myapi.com/foo' ,
 *			method: 'POST',
 *			params: { "id": ":id" }
 *		}]
 *	});
 */
function requestDependencies(e) {
	var completed 	= 0,
		deps 		= e.target.config.deps;

	e.pause();

	for(var i in deps) {
		var dependencyConfig = deps[i];

		//	Register any dep-specific callbacks
		for(var k in dependencyConfig) {
			if(/^on\w+/.test(k)) {
				e.navigator.on('deps:' + k.slice(2), dependencyConfig[k]);
			}
			else if(k === 'on') {
				for(var j in dependencyConfig[k]) e.navigator.on('deps:' + j, dependencyConfig[k][j]);
			}
		}

		if(typeof dependencyConfig === 'string') {
			dependencyConfig = {
				url: dependencyConfig,
				method: 'GET',
				data: {}
			};
		}

		dependencyConfig = clone(dependencyConfig);

		//	We take a look through which params are part of this request and replace
		//	them with the value of the active route
		for(var i in e.params) {
			if(~dependencyConfig.url.indexOf(':' + i)) {
				if(!e.params[i]) {
					dependencyConfig.url = dependencyConfig.url.replace('/:' + i, '');
				}
				else {
					dependencyConfig.url = dependencyConfig.url.replace(':' + i, e.params[i]);
				}
			}
		}

		e.navigator.trigger('deps:before', e, dependencyConfig);

		//	Make our request... (the callback is bound to the scope of the NavigatorEvent)
		ajax(dependencyConfig.url, dependencyConfig.method, dependencyConfig.data).then(function(data, xhr) {
			//	TODO: better error interface...maybe even modularize the whole deps thing...
			if(!/^(2|3)/.test(xhr.status)) {
				e.navigator.trigger('deps:error', xhr);
				return e.cancel();
			}

			e.data = data;

			e.navigator.trigger('deps:complete', e, xhr.responseText, xhr);

			var root = e.navigator.config.deps ? e.navigator.config.deps.root : dependencyConfig.root;

			//	Take all first-level values and transfer them over to the NavigatorEvent's
			//	`data` property.
			if(root && Object.prototype.isPrototypeOf(data)) {
				function recurseObject(obj, key) {
					if(obj[key]) return obj[key];

					for(var k in obj) {
						if(Object.prototype.isPrototypeOf(obj[k])) return recurseObject(obj[k], key) || null;
					}
				}

				e.data = recurseObject(e.data, root);
			}

			if(++completed === deps.length) e.resume();
		});
	}
}