function createWindow() {
	var window = {
		location: {
			pathname: '/'
		},
		document: {
			title: ''
		},
		module: {}
	};

	window.history = {
		stack: ['/'],

		back: function(n) {
			if(n<0) n *= -1;
			while(n-- > 0 && this.stack.length > 1) this.stack.pop();
			window.location.pathname = this.stack[this.stack.length-1];
			if(window.onpopstate) window.onpopstate();
		},

		pushState: function(data, title, path) {
			this.stack.push(path);
			window.location.pathname = path;
		},

		replaceState: function(data, title, path) {
			this.stack.splice(this.stack.length-1,1,path);
			window.location.pathname = path;
		}
	},

	window._xhr = {
		process : null,
		response: '{}',
		status 	: 200,
		headers : {'Content-Type': 'application/json'}
	}

	window.XMLHttpRequest = function() {
		return Object.create({
			url: null,
			method: null,
			data: null,
			async: false,
			readyState: 0,
			status: 200,
			responseText: null,
			responseXML: null,

			getResponseHeader: function(name) {
				return window._xhr.headers[name] || null;
			},

			open: function(method, url, async) {
				this.method = method;
				this.url = url;
				this.async = !!async;
			},

			send: function() {
				var self = this,
					interval;

				if(!this.onreadystatechange) return;

				if(window._xhr.process) window._xhr.process(this);

				this.status = window._xhr.status;
				this.responseText = window._xhr.response;

				if(this.async) {
					interval = setInterval(function() {
						if(++self.readyState === 4) {

							self.onreadystatechange();
							clearInterval(interval);
						}
					}, 100);
				}
				else {
					self.responseText = '{}';
					this.readyState = 4;
					this.onreadystatechange();
				}
			},

			onreadystatechange: null
		});
	}

	return window;
}

module.exports = createWindow;