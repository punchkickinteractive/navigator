//	navigator v0.4.3
//	http://github.com/Punchkick-Interactive/Navigator
//	(c)2013 Punchkick Interactive
//	http://punchkickinteractive.com
//	Navigator may be freely distributed under the MIT license
(function() {
	function extend(obj /*, obj...*/) {
		if(!arguments.length) return;
		var extendedObjects = Array.prototype.slice.call(arguments,1);
		for(var i in extendedObjects) {
			for(var k in extendedObjects[i]) {
				if(obj[k]) {
					if(Array.prototype.isPrototypeOf(obj[k])) {
						obj[k] = obj[k].concat(extendedObjects[i][k]);
					}
					else {
						obj[k] = [obj[k]].concat(extendedObjects[i][k]);
					}
				}
				else {
					obj[k] = extendedObjects[i][k];
				}
			}
		}
		return obj;
	}
	
	function each(list, fn, scope) {
		if(!list) return;
		if(list.forEach) return list.forEach(fn, scope || this);
		for(var i in list) fn.call(scope || this, list[i], i, list);
	}
	
	function clone() {
		var obj = {};
		each(Array.prototype.slice.call(arguments,0), function(source) {
			if (source) {
				for (var prop in source) {
					obj[prop] = source[prop];
				}
			}
		});
		return obj;
	}
	
	function map(list, fn, scope) {
		if(!list) return;
		var output = [], result;
	
		for(var i in list) {
			result = fn.call(scope||this, list[i], /\d+/.test(i) ? parseInt(i,10) : i, list);
			if(typeof i !== 'number') {
				output.push(result);
			}
			else {
				output[i] = result;
			}
		}
		return output;
	}
	
	function result(exp) {
		if(isFunction(exp)) return exp();
		return exp;
	}
	
	function bindAll(obj) {
		for(var i in obj) {
			if(obj.hasOwnProperty(i) && typeof obj[i] === 'function') obj[i] = bind(obj[i], obj);
		}
	
		return obj;
	}
	
	function bind(fn, scope /*, args...*/) {
		if(!fn || !scope) return;
		var args = [];
		if(arguments.length > 2) args = Array.prototype.slice.call(arguments,2);
		return function() {
			return fn.apply(scope, args.concat(Array.prototype.slice.call(arguments,0)));
		}
	}
	
	function isString(obj) {
		return typeof obj === 'string';
	}
	
	function isFunction(obj) {
		return typeof obj === 'function';
	}
	
	function ajax(url, type, data) {
		var xhr = new XMLHttpRequest(),
			callbacks = [],
			onerror = [],
			result, type;
	
		if((!type || type === 'GET')) {
			data = map(data || {}, function(v,k) { return k + '=' + v; }).join('&');
			if(data.length) url += '?' + data;
		}
	
		if(url[url.length-1] === '/') url = url.substr(0,url.length-1);
	
		xhr.open(type || 'GET', url, true);
		xhr.onreadystatechange = function() {
			if(xhr.readyState === 4) {
				if(/^(2|3)/.test(xhr.status)) {
					result = xhr.responseText;
					type = xhr.getResponseHeader('Content-Type');
					if(/json/g.test(type)) {
						try {
							result = JSON.parse(result);
						} catch(e) { result = xhr.responseText }
					}
					while(callbacks.length) callbacks.shift()(result, xhr);
				}
				else {
					while(callbacks.length) callbacks.shift()(null, xhr);
				}
			}
		}
		xhr.send(type === 'POST' ? data : null);
		return {
			then: function then(fn) {
				if(xhr.readystate === 4) return fn(result, xhr);
				callbacks.push(fn);
				return then;
			}
		}
	}
	
	function observable(obj) {
		obj._callbacks = {};
	
		obj.on = function on(name, callback) {
			if(!this._callbacks[name]) this._callbacks[name] = [];
			this._callbacks[name].push(callback);
			return this;
		}
	
		obj.off = function off(name, callback) {
			if(!this_callbacks[name]) return;
			if(!callback) return this._callbacks[name] = [];
			for(var i in this._callbacks[name]) {
				if(callback.name === this._callbacks[name][i].name ||
						callback.toString() === this._callbacks[name][i].toString()) {
					this._callbacks[name].splice(i,1);
					break;
				}
			}
		}
		obj.trigger = function trigger(name, data) {
			for(var i in this._callbacks[name]) {
				this._callbacks[name][i].apply(this, Array.prototype.slice.call(arguments, 1));
			}
	
			for(var i in this._callbacks['all']) {
				this._callbacks['all'][i].apply(this, arguments);
			}
		}
	
		return obj;
	}
	/**
	 * 	Function createNavigatorEvent
	 *
	 * 	Creates a navigatorEvent, which holds an active routes state. The event object
	 *	itself doesn't affect the router globally, only the ACTIVE route. The event
	 * 	object is used for any global callbacks (allowing them to stop/resume/cancel)
	 *	routes that would otherwise be processed.
	 */
	function createNavigatorEvent(Navigator, routeConfig) {
		return bindAll({
			done 		: false, 			// Is this route complete?
			paused 		: false, 			// Is this route paused?
	
			title 		: null, 			// The `title` associated with this route (if a function, it will be evaluated upon `init`)
			hist 		: Navigator.hist,	// A copy of the history stack
			params 		: {}, 				// Dictionary of route parameters and values
			data 		: {}, 				// Data that is populated from any dependencies
			callbacks 	: [], 				// The callback stack for this route
			target 		: routeConfig,
			navigator 	: Navigator,
	
			toString: function() {
				//	return the path of the route when forced to string
				return this.path;
			},
	
			//	Initialize the event by providing the route (from `routes`) that is currently
			//	active.
			init: function(route) {
				this.path = route;
				for(var i in routeConfig.on) Navigator.on(i, routeConfig.on[i]);
				Navigator.trigger('init', this);
				for(var i in routeConfig.callbacks) this.callbacks.push(routeConfig.callbacks[i]);
				this.continue();
				return this;
			},
	
			//	Cancel the route -- this is used when a route dependency errors out.
			cancel: function() {
				Navigator.trigger('cancel', this);
				for(var i in this) this[i] = undefined;
			},
	
			//	Pause the current route in place -- the route will not be fulfilled until
			//	`resume` is called.
			pause: function() {
				if(this.done) return;
				if(!this.paused) this.paused = true;
			},
	
			//	Resumes the routing process if it has been paused
			resume: function() {
				if(this.done) return;
				if(this.paused) this.paused = false;
				this.continue();
			},
	
			//	Continue processing the routes callbacks
			continue: function() {
				if(this.paused) return;
				if(!this.callbacks.length) return this.complete();
				if(this.callbacks.length === routeConfig.callbacks.length) Navigator.trigger('route', this);
				this.callbacks.shift()(this);
				this.continue();
			},
	
			//	Officiate the completion of the route
			complete: function() {
				if(this.callbacks.length) return this.continue();
				Navigator.trigger('complete', this);
				for(var i in routeConfig.on) Navigator.off(i, routeConfig.on[i]);
				this.done = true;
			}
		});
	}
	var exports 		= {},
		landingURL 		= window.location.pathname, //	Might come in handy...
		callbacks 		= observable({}),
		Navigator = bindAll(observable({
			hist: [],
			RouteConfig: {},
			routes: {},
	
			start 		: initialize,
			popState 	: onPopState,
			pushState 	: pushRouteState,
			route 		: addRoute,
			to 			: navigate,
			back 		: back
		}));
	
	//	handle browser forward/backward buttons
	function onPopState() {
		if(window.location.pathname === this.hist[this.hist.length-1]) this.hist.pop();
		this.to(this.hist[this.hist.length-1] || window.location.pathname, true);
	}
	
	//	Triggers popstate, and routes as normal...
	function back() {
		window.history.back(-1);
	}
	
	/**
	 *	Function `pushRouteState`
	 *	This is primarily used by the NavigatorEvent -- given a path, and an optional
	 *	replace parameter, the method will determine how to handle the URL and its
	 *	place in the Navigator `hist` stack.
	 */
	function pushRouteState(navigatorEvent, replace) {
		var route = navigatorEvent;
	
		if(replace) window.history.replaceState({}, result(route.title), route);
		//	If we're not replacing, push this guy into the local Navigator history stack
		if(!replace) {
			if(this.hist[this.hist.length-1] === route) {
				this.hist.pop();
			}
			else if(this.hist[this.hist.length-1] !== window.location.pathname) {
				this.hist.push(window.location.pathname);
			}
	
			window.history.pushState({}, result(route.title), route);
		}
	
		if(this.hist) this.hist = [].concat(this.hist);
	}
	
	//	parse options, if dispatch hasn't explicitely been prevented, navigate the
	//	current `pathname`, and THEN assign the window popstate.
	function initialize(options) {
		this.config = options;
		if(options.routes) for(var i in options.routes) this.route(options.routes[i])
		if(options.dispatch !== false) this.to(window.location.pathname, true);
		this.trigger('start', this);
		window.onpopstate = this.popState;
		this.on('route', this.pushState);
	}
	
	/**
	 *	Function `addRoute`
	 *		addRoute(route, callback...)
	 *		addRoute(title, route, callback...)
	 *
	 *	The `title` parameter is optional, and if included, will be used only to set
	 *	the active route context paramter `title`, in which case a provided callback
	 *	can just call `document.title = this.title`.
	 *
	 *	Routes can be exact strings, strings with required or optional named parameters,
	 *	OR strings with regular expression named paramters. Examples below:
	 *		/blog 							--	no parameters
	 *		/blog/articles/:article_id		--	with a reqired parameter
	 *		/blog/articles/:article_id?		-- 	with an optional parameter
	 *		/blog/articles/:month=(\\w+)	-- 	with a required regexp paramter
	 *
	 *	* Non-regular expression parameters will match any number of valid URL characters
	 */
	function addRoute(route, callback/*[, callback...]*/) {
		var route = route, title, config;
	
		//	Handle both configuration object and a simple route...
		if(Object.prototype.isPrototypeOf(arguments[0])) {
			var config = route;
			title = config.title;
			route = config.path;
		}
		else {
			config = {path: route};
		}
	
		//	Matches a string like '/directory/:ggp=(\\w+)/:malls?'
		//	and if we `match` its output is [":ggp=(\\w+)", ":malls?"]
		var pattern 	= route,
			//	match all groups (including their optional/regexp flags)
			groups 		= pattern.match(/:([-\w_\d]+)(=[\-\\\[\]\w\d\+\*\?\(\)\{\}\,\s]+)?\??/g),
			//	string-ized regexp of ALL valid URL characters
			UrlRE 		= "([-\\w\\d\\.\\_\\~\\:\\/\\?\\#\\[\\]\\@\\!\\$\\&\\'\\(\\)\\*\\+\\,\\;\\=]+)",
			//	since the title is optional, we need to determine where to slice the arguments for callbacks...
			callbacks 	= config.callbacks || Array.prototype.slice.call(arguments, 1);
	
		for(var g in groups) {
			var routeParams = groups[g].split(/(\?|\=)/), 	// split on optional or regexp paths
				groupRE 	= groups[g];
	
			if(!!routeParams[2]) {
				groupRE = routeParams[2];  // route regexp value
			}
			else {
				groupRE = UrlRE;
			}
	
			if(~routeParams.indexOf('?')) 	groupRE += '?';  // if optional ignore preceeding '/' before param
			pattern = pattern.replace(groups[g], '?' + groupRE); 	// overwrite group in route pattern with the real RE (also, make preceeding '/' optional)
	
			groups[g] = {
				name 		: routeParams[0].substr(1), 	// get rid of preceeding ':'
				isOptional	: !!~routeParams.indexOf('?'),
				isRegExp 	: !!routeParams[2],
				re  		: new RegExp(groupRE)
			};
		}
	
		this.routes[pattern] = {
			config  	: config,
			path 		: route,
			groups 		: groups,
			callbacks 	: callbacks
		};
	
		return this;
	}
	
	/**
	 *	Function navigate
	 *
	 *	Attempts to match a given path to a registered route. The optional replace
	 *	argument is a boolean, in which its value refers to invoking the `history.pushState`
	 *	and `history.replaceState` methods.
	 */
	function navigate(route, replace) {
		var re, groups, params, values, evt, match,
			noRequiredGroupValue = false, hasRoute = false;
	
		//	If we're not replacing, don't re-route a route! Ya dummy!
		if(!replace && window.location.pathname === route) return;
	
		matches = [];
	
		for(var r in this.routes) {
			hasRoute = true;
			//	Create a RE out of our generated route RE
			re = new RegExp('^'+r+'$', 'g');
			if(re.test(route)) matches.push(this.routes[r]);
		}
	
		//	Sort the routes by most specific first.
		matches.sort(function(a, b) {
			if(a.path.length > b.path.length) return -1;
			if(a.path.length < b.path.length) return 1;
			return 0;
		});
	
		//	Go through each matched route until we find one that fulfills all properties
		if(matches.length) {
			for(var i in matches) {
				evt = createNavigatorEvent(this, matches[i]);
	
				groups 		= matches[i].groups;	//	Get all/any parameter groups
				routeChunks = route.split('/').slice(2);
				params 		= {}; 					// 	Our params object, for the NavigatorEvent
				values 		= route.match(re); 		//	The values for our params
	
				//	Get the values for our group names, and populate the params dictionary
				each(groups, function(group, i) {
					//	Match each groupname with it's respective match
					//	within the passed url.
					evt.params[group.name] = routeChunks[i] && routeChunks[i].match(group.re)[1] || null;
	
					//	If a non-optional parameter was not populated, tell navigator so...
					if(!evt.params[group.name] && !group.isOptional) noRequiredGroupValue = true;
				});
	
				if(noRequiredGroupValue) {
					//	Reset the variable
					noRequiredGroupValue = false;
					//	and continue on testing routes...
					continue;
				}
	
				this.trigger('before', evt);
				evt.init(route);
				//	Since we found a match, break on out (any other routes are discarded)
				break;
			}
		}
	}
	Navigator.on('before', function(e) {
		if(e.target.config.deps) e.callbacks.push(requestDependencies);
	});
	
	/**
	 *	Function `requestDependencies`
	 *	Makes requests based on route `deps` values. The values can be a
	 *	string (i.e. http://myapi.com/foo), or an object with more specifics pertaining
	 *	to the AJAX call -- this outputs a curried call to the appropriate dependency call.
	 *
	 *	// Simple Dependency
	 *	Navigator.route({
	 *		path: '/foo',
	 *		deps: [ 'http://myapi.com/foo' ]
	 *	});
	 *
	 *  // Configured Dependency
	 *	Navigator.route({
	 *		path: '/foo',
	 *		deps: [{
	 * 			url: 'http://myapi.com/foo' ,
	 *			method: 'POST',
	 *			params: { "id": ":id" }
	 *		}]
	 *	});
	 */
	function requestDependencies(e) {
		var completed 	= 0,
			deps 		= e.target.config.deps;
	
		e.pause();
	
		for(var i in deps) {
			var dependencyConfig = deps[i];
	
			//	Register any dep-specific callbacks
			for(var k in dependencyConfig) {
				if(/^on\w+/.test(k)) {
					e.navigator.on('deps:' + k.slice(2), dependencyConfig[k]);
				}
				else if(k === 'on') {
					for(var j in dependencyConfig[k]) e.navigator.on('deps:' + j, dependencyConfig[k][j]);
				}
			}
	
			if(typeof dependencyConfig === 'string') {
				dependencyConfig = {
					url: dependencyConfig,
					method: 'GET',
					data: {}
				};
			}
	
			dependencyConfig = clone(dependencyConfig);
	
			//	We take a look through which params are part of this request and replace
			//	them with the value of the active route
			for(var i in e.params) {
				if(~dependencyConfig.url.indexOf(':' + i)) {
					if(!e.params[i]) {
						dependencyConfig.url = dependencyConfig.url.replace('/:' + i, '');
					}
					else {
						dependencyConfig.url = dependencyConfig.url.replace(':' + i, e.params[i]);
					}
				}
			}
	
			e.navigator.trigger('deps:before', e, dependencyConfig);
	
			//	Make our request... (the callback is bound to the scope of the NavigatorEvent)
			ajax(dependencyConfig.url, dependencyConfig.method, dependencyConfig.data).then(function(data, xhr) {
				//	TODO: better error interface...maybe even modularize the whole deps thing...
				if(!/^(2|3)/.test(xhr.status)) {
					e.navigator.trigger('deps:error', xhr);
					return e.cancel();
				}
	
				e.data = data;
	
				e.navigator.trigger('deps:complete', e, xhr.responseText, xhr);
	
				var root = e.navigator.config.deps ? e.navigator.config.deps.root : dependencyConfig.root;
	
				//	Take all first-level values and transfer them over to the NavigatorEvent's
				//	`data` property.
				if(root && Object.prototype.isPrototypeOf(data)) {
					function recurseObject(obj, key) {
						if(obj[key]) return obj[key];
	
						for(var k in obj) {
							if(Object.prototype.isPrototypeOf(obj[k])) return recurseObject(obj[k], key) || null;
						}
					}
	
					e.data = recurseObject(e.data, root);
				}
	
				if(++completed === deps.length) e.resume();
			});
		}
	}
	Navigator.on('route', function(e) {
		if(e.target.config.title) {
			e.title = (typeof e.target.config.title === 'function') ? e.target.config.title.apply(this, arguments) : e.target.config.title;
		}
	});
	module.exports =  Navigator;
})();