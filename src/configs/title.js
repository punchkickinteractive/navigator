Navigator.on('route', function(e) {
	if(e.target.config.title) {
		e.title = (typeof e.target.config.title === 'function') ? e.target.config.title.apply(this, arguments) : e.target.config.title;
	}
});