function extend(obj /*, obj...*/) {
	if(!arguments.length) return;
	var extendedObjects = Array.prototype.slice.call(arguments,1);
	for(var i in extendedObjects) {
		for(var k in extendedObjects[i]) {
			if(obj[k]) {
				if(Array.prototype.isPrototypeOf(obj[k])) {
					obj[k] = obj[k].concat(extendedObjects[i][k]);
				}
				else {
					obj[k] = [obj[k]].concat(extendedObjects[i][k]);
				}
			}
			else {
				obj[k] = extendedObjects[i][k];
			}
		}
	}
	return obj;
}

function each(list, fn, scope) {
	if(!list) return;
	if(list.forEach) return list.forEach(fn, scope || this);
	for(var i in list) fn.call(scope || this, list[i], i, list);
}

function clone() {
	var obj = {};
	each(Array.prototype.slice.call(arguments,0), function(source) {
		if (source) {
			for (var prop in source) {
				obj[prop] = source[prop];
			}
		}
	});
	return obj;
}

function map(list, fn, scope) {
	if(!list) return;
	var output = [], result;

	for(var i in list) {
		result = fn.call(scope||this, list[i], /\d+/.test(i) ? parseInt(i,10) : i, list);
		if(typeof i !== 'number') {
			output.push(result);
		}
		else {
			output[i] = result;
		}
	}
	return output;
}

function result(exp) {
	if(isFunction(exp)) return exp();
	return exp;
}

function bindAll(obj) {
	for(var i in obj) {
		if(obj.hasOwnProperty(i) && typeof obj[i] === 'function') obj[i] = bind(obj[i], obj);
	}

	return obj;
}

function bind(fn, scope /*, args...*/) {
	if(!fn || !scope) return;
	var args = [];
	if(arguments.length > 2) args = Array.prototype.slice.call(arguments,2);
	return function() {
		return fn.apply(scope, args.concat(Array.prototype.slice.call(arguments,0)));
	}
}

function isString(obj) {
	return typeof obj === 'string';
}

function isFunction(obj) {
	return typeof obj === 'function';
}

function ajax(url, type, data) {
	var xhr = new XMLHttpRequest(),
		callbacks = [],
		onerror = [],
		result, type;

	if((!type || type === 'GET')) {
		data = map(data || {}, function(v,k) { return k + '=' + v; }).join('&');
		if(data.length) url += '?' + data;
	}

	if(url[url.length-1] === '/') url = url.substr(0,url.length-1);

	xhr.open(type || 'GET', url, true);
	xhr.onreadystatechange = function() {
		if(xhr.readyState === 4) {
			if(/^(2|3)/.test(xhr.status)) {
				result = xhr.responseText;
				type = xhr.getResponseHeader('Content-Type');
				if(/json/g.test(type)) {
					try {
						result = JSON.parse(result);
					} catch(e) { result = xhr.responseText }
				}
				while(callbacks.length) callbacks.shift()(result, xhr);
			}
			else {
				while(callbacks.length) callbacks.shift()(null, xhr);
			}
		}
	}
	xhr.send(type === 'POST' ? data : null);
	return {
		then: function then(fn) {
			if(xhr.readystate === 4) return fn(result, xhr);
			callbacks.push(fn);
			return then;
		}
	}
}

function observable(obj) {
	obj._callbacks = {};

	obj.on = function on(name, callback) {
		if(!this._callbacks[name]) this._callbacks[name] = [];
		this._callbacks[name].push(callback);
		return this;
	}

	obj.off = function off(name, callback) {
		if(!this_callbacks[name]) return;
		if(!callback) return this._callbacks[name] = [];
		for(var i in this._callbacks[name]) {
			if(callback.name === this._callbacks[name][i].name ||
					callback.toString() === this._callbacks[name][i].toString()) {
				this._callbacks[name].splice(i,1);
				break;
			}
		}
	}
	obj.trigger = function trigger(name, data) {
		for(var i in this._callbacks[name]) {
			this._callbacks[name][i].apply(this, Array.prototype.slice.call(arguments, 1));
		}

		for(var i in this._callbacks['all']) {
			this._callbacks['all'][i].apply(this, arguments);
		}
	}

	return obj;
}